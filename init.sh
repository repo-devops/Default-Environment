#!/bin/sh
xrandr --newmode "800x600_60.00" 38.22 800 832 912 1024 600 601 604 622 -HSync +Vsync
xrandr --addmode screen "800x600_60.00"
xrandr -s "800x600_60.00"
chmod +x 'bin/ffmpeg/ffmpeg' 'bin/ffmpeg/ffprobe'
chmod +x 'bin/rar/rar' 'bin/rar/unrar'
chmod +x 'bin/ffextract' 'bin/ffreplace'
chmod +x 'bin/megaresume'
rm -r google-chrome.config
rm -r google-chrome.cache
rm google_chrome-amd64.deb
rm init.sh
lxpanel &> /dev/null &
