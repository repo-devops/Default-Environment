FROM gitpod/workspace-full-vnc

USER gitpod
COPY google_chrome-amd64.deb /tmp

#ALL
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal main restricted universe multiverse' > /etc/apt/sources.list"
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal-security main restricted universe multiverse' >> /etc/apt/sources.list"
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal-updates main restricted universe multiverse' >> /etc/apt/sources.list"
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal-proposed main restricted universe multiverse' >> /etc/apt/sources.list"
RUN sudo sh -c "echo 'deb http://us.archive.ubuntu.com/ubuntu focal-backports main restricted universe multiverse' >> /etc/apt/sources.list"
RUN sudo apt-get -q update

RUN sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install x11-xserver-utils libnss3 gdebi arj lzip lzma lzop ncompress p7zip-full rpm2cpio rzip sharutils squashfs-tools unace unalz unar xz-utils ffmpeg
RUN sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install lxpanel pcmanfm file-roller deluge winff

RUN sudo sh -c "gdebi -n /tmp/google_chrome-amd64.deb"

COPY 'google-chrome.config' '/home/gitpod/.config/google-chrome'
RUN sudo sh -c "chown -R gitpod:gitpod '/home/gitpod/.config/google-chrome'"
RUN sudo sh -c "chmod -R 0755 '/home/gitpod/.config/google-chrome'"

COPY 'google-chrome.cache' '/home/gitpod/.cache/google-chrome'
RUN sudo sh -c "chown -R gitpod:gitpod '/home/gitpod/.cache/google-chrome'"
RUN sudo sh -c "chmod -R 0755 '/home/gitpod/.cache/google-chrome'"

COPY init.sh /tmp/init.sh
RUN sudo sh -c "mv '/tmp/init.sh' '/etc/init.sh'"
RUN sudo sh -c "chown gitpod:gitpod '/etc/init.sh'"
RUN sudo sh -c "chmod 0755 '/etc/init.sh'"

USER root
